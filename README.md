## Kubernetes auto-deployments by Frontmen

### Remarks

This project is based on https://gitlab.com/gitlab-org/gitlab-ci-yml/blob/master/autodeploy/Kubernetes.gitlab-ci.yml

### License

MIT, GitLab, 2016-2017
