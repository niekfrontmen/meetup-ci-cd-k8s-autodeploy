#!/usr/bin/env bash

echo "-> START BUILDING OUR APPLICATION"
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
	DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
	SOURCE="$(readlink "$SOURCE")"
	[[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done

export DEPLOY_ROOT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

source "$DEPLOY_ROOT_DIR/src/common.bash"

echo "	- CHECKING DOCKER ENGINE"
ensure_docker_engine
echo "	- SUCCESS CHECKING DOCKER ENGINE"

docker rm -f "$CI_CONTAINER_NAME" &>/dev/null || true

echo "-> START BUILDING THE APPLICATION"

if [[ -f Dockerfile ]]; then
	echo "	- BUILDING DOCKER-FILE BASED APPLICATION"
	# Build Dockerfile
	docker build -t "$CI_REGISTRY_IMAGE:$CI_REGISTRY_TAG" .
else
	# Build heroku-based application
	echo "	- BUILDING HEROKU BASED APPLICATION USING GLIDERLABS/HEROKUISH DOCKER IMAGE"
	docker run -i --name="$CI_CONTAINER_NAME" \
		-v "$(pwd):/tmp/app:ro" \
		-v "/cache/herokuish:/tmp/cache" \
		gliderlabs/herokuish /bin/herokuish buildpack build
	docker commit "$CI_CONTAINER_NAME" "$CI_REGISTRY_IMAGE:$CI_REGISTRY_TAG"
	docker rm "$CI_CONTAINER_NAME" >/dev/null
	echo ""

	# Create a start command, start `web`
	echo "	- CONFIGURING $CI_REGISTRY_IMAGE:$CI_REGISTRY_TAG DOCKER IMAGE"
	docker create --expose 5000 --env PORT=5000 \
		--name="$CI_CONTAINER_NAME" \
		"$CI_REGISTRY_IMAGE:$CI_REGISTRY_TAG" \
		/bin/herokuish procfile start web
	docker commit "$CI_CONTAINER_NAME" "$CI_REGISTRY_IMAGE:$CI_REGISTRY_TAG"
	docker rm "$CI_CONTAINER_NAME" >/dev/null
	echo ""
fi

if [[ -n "$CI_BUILD_TOKEN" ]]; then
	echo "	- LOGGING INTO GITLAB CONTAINER REGISTRY WITH THE GITLAB CREDENTIALS"
	docker login -u gitlab-ci-token -p "$CI_BUILD_TOKEN" "$CI_REGISTRY"
	echo ""
fi

echo "	- PUSHING IMAGE TO GITLAB CONTAINER REGISTRY"
docker push "$CI_REGISTRY_IMAGE:$CI_REGISTRY_TAG"
echo "  - FINSHED BUILDING THE APPLICATION IMAGE AND PUSHING IT TO THE REGISTRY"
echo "#################################################################################"
echo ""
