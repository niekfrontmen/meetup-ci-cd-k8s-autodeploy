set -eo pipefail

[[ "$TRACE" ]] && set -x

export CI_CONTAINER_NAME="ci_job_build_$CI_BUILD_ID"
export CI_REGISTRY_TAG="$CI_BUILD_REF_SLUG"
echo ""
echo "### STARTING AUTO DEPLOY IMAGE FOR THE EINDHOVEN.JS MEETUP BY FRONTMEN - SPRING EDITION ###"
echo "==========================================================================================="

# Let's create the K8S Configuration and set the context to use the Gitlab Deploy to allow interaction between this image, 
# our Gitlab Repo and our K8S cluster
create_kubeconfig() {
  echo "  -> STARTED GENERATING KUBECONFIG SO WE CAN COMMUNICATE WITH OUR GITLAB REPOSITORY"
  echo "    - GET THE K8S CA AUTHORITY PEM CERTIFICATE"
  [[ -z "$KUBE_URL" ]] && return
  export KUBECONFIG="$(pwd)/kubeconfig"
  export KUBE_CLUSTER_OPTIONS=
  if [[ -n "$KUBE_CA_PEM" ]]; then
    echo "    - FOUND THE PEM CERTIFICATE"
    echo "    - STORE THE PEM CERTIFICATE IN THE CLUSTER OPTIONS OBJECT"
    echo "$KUBE_CA_PEM" > "$(pwd)/kube.ca.pem"
    export KUBE_CLUSTER_OPTIONS=--certificate-authority="$(pwd)/kube.ca.pem"
  fi
  echo "    - STORE THE K8S URL ($KUBE_URL) IN THE CLUSTER OPTIONS"
  kubectl config set-cluster gitlab-deploy --server="$KUBE_URL" \
    $KUBE_CLUSTER_OPTIONS

  echo "    - STORE THE K8S SERVICETOKEN IN THE CLUSTER OPTIONS"
  kubectl config set-credentials gitlab-deploy --token="$KUBE_TOKEN" \
    $KUBE_CLUSTER_OPTIONS
  kubectl config set-context gitlab-deploy \
    --cluster=gitlab-deploy --user=gitlab-deploy \
    --namespace="$KUBE_NAMESPACE"
  echo "-> FINISHED GENERATING KUBECONFIG. SET THE CONTEXT TO GITLAB DEPLOY SO WE CAN START INTERACT WITH OUR GITLAB REPO"
  kubectl config use-context gitlab-deploy
  echo "    #############################################################################"
  echo ""
}

# Ensure we retrieve the Environment URL & Port we specified in our .gitlab-ci.yml file
ensure_environment_url() {
  echo "  -> STARTED ENSURING WE HAVE AN ENVIRONMENT URL SPECIFIED IN OUR gitlab-ci.yml-FILE"

  CI_ENVIRONMENT_URL="$(ruby -ryaml -e 'puts YAML.load_file(".gitlab-ci.yml")[ENV["CI_BUILD_NAME"]]["environment"]["url"]')"
  CI_ENVIRONMENT_URL="$(eval echo "$CI_ENVIRONMENT_URL")"

  echo "    - ENVIRONMENT URL STORED IN CI_ENVIRONMENT_URL-VARIABLE: $CI_ENVIRONMENT_URL"
  echo "    - FINISHED ENSURING WE HAVE AN ENVIRONMENT URL"
  echo "    #############################################################################"
  echo ""
}

# Verfify we have all the necessary variables for deployment to K8S
ensure_deploy_variables() {
  echo "  -> STARTED CHECKING IF WE HAVE ALL VARIABLES NECESSARY FOR DEPLOYMENT TO K8S"
  echo "    - START CHECKING KUBE_NAMESPACE"
  if [[ -z "$KUBE_NAMESPACE" ]]; then
    echo "      - ERROR: MISSING KUBE_NAMESPACE"
    exit 1
  fi
  echo "      - SUCCESS: NAMESPACE FOUND ($KUBE_NAMESPACE)"

  echo "    - START CHECKING CI_ENVIRONMENT_SLUG"
  if [[ -z "$CI_ENVIRONMENT_SLUG" ]]; then
    echo "      - ERROR: MISSING CI_ENVIRONMENT_SLUG"
    exit 1
  fi
  echo "      - SUCCESS: CI_ENVIRONMENT_SLUG FOUND ($CI_ENVIRONMENT_SLUG)"

  echo "    - START CHECKING CI_ENVIRONMENT_URL"
  if [[ -z "$CI_ENVIRONMENT_URL" ]]; then
    echo "      - ERROR: MISSING CI_ENVIRONMENT_URL"
    exit 1
  fi
  echo "      - SUCCESS: CI_ENVIRONMENT_URL FOUND ($CI_ENVIRONMENT_URL)"

  echo "    - FINISHED ENSURING WE HAVE THE DEPLOYMENT VARIABLES"
  echo "  #############################################################################"
  echo ""
}

# Ping the K8S Cluster to see if it's available
ping_kube() {
  echo "  -> STARTED PINGING THE K8S CLUSTER"
  if kubectl version > /dev/null; then
    echo "    - SUCCESS: K8S CLUSTER IS ONLINE"
    echo ""
  else
    echo "    - ERROR: K8S CLUSTER IS NOT AVAILABLE"
    echo ""
    return 1
  fi
  echo "  - FINISHED PINGING THE K8S CLUSTER"
  echo "#################################################################################"
  echo ""
}

# Verify the Docker Engine is available
ensure_docker_engine() {
  echo "  -> START VERIFYING THE DOCKER ENGINE IS AVAILABLE"
  if ! docker info &>/dev/null; then
    echo "    - ERROR: MISSING DOCKER ENGINE TO BUILD IMAGES"
    echo "    - RUNNING DOCKER:DIND LOCALLY WITH CACHE DRIVER"

    if ! grep -q overlay /proc/filesystems; then
      echo "    - ERROR: MISSING OVERLAY FILESYSTEM"
      exit 1
    fi

    if [[ ! -d /cache ]]; then
      mkdir -p /cache
      mount -t tmpfs tmpfs /cache
    fi

    dockerd \
      --host=unix:///var/run/docker.sock \
      --storage-driver=overlay \
      --data-root=/cache/docker & &>/docker.log

    trap 'kill %%' EXIT

    echo "    - WAITING ON DOCKER..."
    for i in $(seq 1 60); do
      if docker info &> /dev/null; then
        break
      fi
      sleep 1s
    done

    if [[ "$i" == 60 ]]; then
      echo "    - ERROR: FAILED TO START DOCKER:DIND"
      cat /docker.log
      exit 1
    fi
    echo ""
  fi
  echo "  - FINSHED VERIFYING THE DOCKER ENGINE IS AVAILABLE"
  echo "#################################################################################"
  echo ""
}
